<section class="banner-tems text-center">
    <div class="container">
        <div class="banner-content">
            <h2><?php
                $value = $data['data'][0];
                if (isset($value['room_name']) && ($value['room_name'] != "N/A") && ($value['room_name'] != NULL)) {
                    echo '<strong>' . $value['room_no'] . "</strong><br/>" . '<span style="color:#000077;">' . $value['room_name'] . '</span>';
                } else {
                    echo "Room No. " . $value['room_no'];
                }     ?></h2>
        </div>
    </div>
</section>
<!-- ROOM DETAIL -->
<section class="section-product-detail">
    <div class="container">
        <!-- DETAIL -->
        <div class="product-detail margin">
            <div class="row">
                <div class="col-lg-9">
                    <!-- LAGER IMGAE -->
                  
                    <div class="wrapper">
                            <div class="gallery3">
                                <div class="gallery__img-block  ">
                                    <span class="">
                                    Hotel Main View Room
                                 </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  current">
                                    <span class="">
                                        Hotel Main View Room 1
                                     </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                        Hotel Main View Room 2
                                     </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                        Hotel Main View Room 3
                                     </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                         Hotel Main View Room 4
                                      </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                    Hotel Main View Room 5
                                 </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                      Hotel Main View Room 6
                                   </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                     Hotel Main View Room 7
                                  </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                       Hotel Main View Room 8
                                    </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__img-block  ">
                                    <span class="">
                                      Hotel Main View Room 9
                                   </span>
                                    <img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" class="">
                                </div>
                                <div class="gallery__controls">
                                </div>
                            </div>
                        </div>
                    
                    <!-- END / LAGER IMGAE -->
                </div>
                <div class="col-lg-3">
                    <!-- FORM BOOK -->
                    <div class="product-detail_book">
                        <div class="product-detail_total">
                            <img src="<?php echo base_url(); ?>assets/images/Product/icon.png" alt="#" class="icon-logo">
                            <h6><?php echo $data['data'][0]['room_name']; ?></h6>
                            <p class="price">
                                <span class="amout">₹<?php echo $data['data'][0]['room_rent']; ?></span> /day
                            </p>
                        </div>
                        <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-danger" role="alert">
                            <?php echo $this->session->flashdata('message'); ?>
                            </div>
                           
                        <?php } ?>
                        <form action="<?php echo base_url('checkout'); ?>" method="post">
                            <input type="hidden" name="rmid" value="<?php echo $data['data'][0]['room_id']; ?>">
                            <div class="product-detail_form">
                                <div class="sidebar">
                                    <!-- WIDGET CHECK AVAILABILITY -->
                                    <div class="widget widget_check_availability">
                                        <div class="check_availability">
                                            <div class="check_availability-field">
                                                <label>Arrive</label>
                                                <div class="input-group date" data-date-format="dd-mm-yyyy" id="datepicker1">
                                                    <input class="form-control wrap-box" type="text" placeholder="Arrival Date" name="std" required>
                                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="check_availability-field">
                                                <label>Depature</label>
                                                <div id="datepicker2" class="input-group date" data-date-format="dd-mm-yyyy">
                                                    <input class="form-control wrap-box" type="text" placeholder="Departure Date" name="end" required>
                                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="check_availability-field">
                                                <label>Adult</label>
                                                <select class="awe-select" name="adult" required>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="check_availability-field">
                                                <label>Child</label>
                                                <select class="awe-select" name="child" required>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="check_availability-field">
                                                <label>Select State</label>
                                                <select class="awe-select" name="state_id" required>
                                                    <?php for ($i = 0; $i < count($data['state_data']); $i++) { ?>
                                                        <option value="<?php echo $data['state_data'][$i]['state_id']; ?>"><?php echo $data['state_data'][$i]['Name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END / WIDGET CHECK AVAILABILITY -->
                                </div>
                                <button type="submit" class="btn btn-room btn-product">Book Now</button>
                            </div>
                        </form>
                    </div>
                    <!-- END / FORM BOOK -->
                </div>
            </div>
        </div>
        <!-- END / DETAIL -->
        <!-- TAB -->
        <div class="product-detail_tab">
            <div class="row">
                <div class="col-md-3">
                    <ul class="product-detail_tab-header">
                        <li class="active"><a href="#overview" data-toggle="tab">OVERVIEW</a></li>
                        <!-- <li><a href="#package" data-toggle="tab">RATE</a></li> -->
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="product-detail_tab-content tab-content">
                        <!-- OVERVIEW -->
                        <div class="tab-pane active" id="overview">
                            <div class="product-detail_overview">

                                <?php
                                if ($value['room_features']) {
                                    $room_feature_array = explode(",", $value['room_features']);
                                    $room_features_string = "";
                                    for ($i = 0; $i < count($room_feature_array); $i++) {
                                        for ($j = 0; $j < count($data['room_fetature_data']); $j++) {
                                            if ($room_feature_array[$i] == $data['room_fetature_data'][$j]['room_feature_id']) {
                                                $room_features_string .= $data['room_fetature_data'][$j]['room_feature_name'] . ",";
                                            }
                                        }
                                    }
                                } else {
                                    $room_features_string = "";
                                }
                                ?>
                                <h5 class='text-uppercase
                        '><?php echo $room_features_string; ?></h5>
                                <div class="row">
                                    <div class="col-xs-6 col-md-4">
                                        <h6>SPECIAL ROOM</h6>
                                        <ul>
                                            <li>Max: <?php echo $value['max_occupancy']; ?> Person(s)</li>
                                            <li>Floor: <?php if ($value['floor_no'] == 0)
                                                            echo 'Ground Floor';
                                                        else if ($value['floor_no'] % 10 == 1)
                                                            echo $value['floor_no'] . 'st Floor';
                                                        else if ($value['floor_no'] % 10 == 2)
                                                            echo $value['floor_no'] . 'nd Floor';
                                                        else if ($value['floor_no'] % 10 == 3)
                                                            echo $value['floor_no'] . 'rd Floor';
                                                        else
                                                            echo $value['floor_no'] . 'th Floor'; ?></li>
                                            <li>Rate Type: <?php $rate_type = ($value['floor_no'] == 0) ? "Fixed" : "Depends";
                                                            echo $rate_type;  ?></li>
                                            <li>Bed:
                                                <?php
                                                if ($value['room_bed'] == 1) {
                                                    echo "Single Bed ";
                                                } else if ($value['room_bed'] == 2) {
                                                    echo "Double Bed ";
                                                } else if ($value['room_bed'] == 3) {
                                                    echo "Tripple Bed ";
                                                } else
                                                    echo $value['room_bed'] . " Bed ";

                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / OVERVIEW -->
                        <!-- PACKAGE -->
                        <div class="tab-pane fade" id="package">
                            <div class="product-detail_package">
                                <!-- ITEM package -->
                                <div class="product-package_item">
                                    <div class="text">
                                        <h4><a href="#">normal</a></h4>
                                        <div class="product-package_price">
                                            <p class="price">
                                                <span class="amout">₹<?php echo $data['data'][0]['room_rent']; ?></span> / day
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- END / ITEM package -->
                                <!-- ITEM package -->
                                <div class="product-package_item">
                                    <div class="text">
                                        <h4><a href="#">weekend</a></h4>
                                        <div class="product-package_price">
                                            <p class="price">
                                                <span class="amout">₹<?php echo $data['data'][0]['room_rent_weekend']; ?></span> / day
                                            </p>

                                        </div>
                                    </div>
                                </div>
                                <!-- END / ITEM package -->
                                <!-- ITEM package -->
                                <div class="product-package_item">
                                    <div class="text">
                                        <h4><a href="#">seasonal</a></h4>
                                        <div class="product-package_price">
                                            <p class="price">
                                                <span class="amout">₹<?php echo $data['data'][0]['room_rent_seasonal']; ?></span> / day
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- END / ITEM package -->
                            </div>
                        </div>
                        <!-- END / PACKAGE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END / TAB -->
    </div>
</section>
<!-- END / SHOP DETAIL -->