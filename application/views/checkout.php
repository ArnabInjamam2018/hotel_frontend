<section class="banner-tems text-center">
    <div class="container">
        <div class="banner-content">
            <h2>RESERVATION</h2>
        </div>
    </div>
</section>
<!-- RESERVATION -->
<section class="section-reservation-page ">
    <div class="container">
        <div class="reservation-page">
            <div class="row">
                <!-- SIDEBAR -->
                <div class="col-md-4 col-lg-3">
                    <div class="reservation-sidebar">
                        <!-- RESERVATION DATE -->
                        <div class="reservation-date">
                            <!-- HEADING -->
                            <h2 class="reservation-heading">Dates</h2>
                            <!-- END / HEADING -->
                            <ul>
                                <li>
                                    <span>Check-In</span>
                                    <span><?php echo date('d-m-Y',strtotime($data['check_in']['date']));?></span>
                                </li>
                                <li>
                                    <span>Check-Out</span>
                                    <span><?php echo date('d-m-Y',strtotime($data['check_out']['date']));?></span>
                                </li>
                                <li>
                                    <span>Total Nights</span>
                                    <span><?php echo $data['number_of_days']; ?></span>
                                </li>
                                <li>
                                    <span>Total Room</span>
                                    <span>1</span>
                                </li>
                                <li>
                                    <span>Total Guests</span>
                                    <span><?php echo $data['adult']; ?> Adults <?php echo $data['child']; ?> Children</span>
                                </li>
                            </ul>
                        </div>
                        <!-- END / RESERVATION DATE -->
                        <!-- ROOM SELECT -->
                        <div class="reservation-room-selected selected-4 ">
                            <!-- HEADING -->
                            <h2 class="reservation-heading">Room Details</h2>
                            <!-- END / HEADING -->
                            <!-- ITEM -->
                            <div class="reservation-room-seleted_item">
                                <h6>ROOM No. <?php echo $data['room_array'][0]['room_no']; ?></h6> <span class="reservation-option"><?php echo $data['adult']; ?> Adult, <?php echo $data['child']; ?> Child</span>
                                <div class="reservation-room-seleted_name has-package">
                                </div>
                                <div class="reservation-room-seleted_package">
                                    <h6>Space Price</h6>
                                    <ul>
                                        <li>
                                            <span>Room Charge</span>
                                            <span>₹<?php echo $data['total_with_out_tax']; ?></span>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>SGST</span>
                                            <span>₹<?php echo $data['sgst_amount']; ?></span>
                                        </li>
                                        <li>
                                            <span>CGST</span>
                                            <span>₹<?php echo $data['cgst_amount']; ?></span>
                                        </li>
                                        <li>
                                            <span>Total Tax</span>
                                            <span>₹<?php echo $data['total_tax_amount']; ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                            <!-- TOTAL -->
                            <div class="reservation-room-seleted_total ">
                                <label>TOTAL</label>
                                <span class="reservation-total">₹<?php echo $data['all_total']; ?></span>
                            </div>
                            <!-- END / TOTAL -->
                        </div>
                        <!-- END / ROOM SELECT -->
                    </div>
                </div>
                <!-- END / SIDEBAR -->
                <!-- CONTENT -->
                <div class="col-md-8 col-lg-9">
                    <div class="reservation_content">
                        <div class="reservation-billing-detail">
                            <h4>BILLING DETAILS</h4>
                            <label>Country <sup> *</sup></label>
                            <select class="awe-select">
                                <option>United Kingdom (Uk)</option>
                                <option>Viet Nam</option>
                                <option>Thai Lan</option>
                                <option>China</option>
                            </select>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>First Name<sup> *</sup></label>
                                    <input type="text" class="input-text">
                                </div>
                                <div class="col-sm-6">
                                    <label>Last Name<sup> *</sup></label>
                                    <input type="text" class="input-text">
                                </div>
                            </div>
                            <label>Address<sup> *</sup></label>
                            <input type="text" class="input-text" placeholder="Street Address">
                            <br>
                            <br>
                            <input type="text" class="input-text" placeholder="Apartment, suite, unit etc. (Optional)">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Town / City<sup> *</sup></label>
                                    <input type="text" class="input-text" placeholder="Street Address">
                                </div>
                                <div class="col-sm-6">
                                    <label>Country<sup> *</sup></label>
                                    <input type="text" class="input-text" placeholder="Country">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Email Address<sup> *</sup></label>
                                    <input type="text" class="input-text" placeholder="Street Address">
                                </div>
                                <div class="col-sm-6">
                                    <label>Phone<sup> *</sup></label>
                                    <input type="text" class="input-text" placeholder="Country">
                                </div>
                            </div>
                            <label>Order Notes</label>
                            <textarea class="input-textarea" placeholder="Notes about your order, eg. special notes for delivery"></textarea>
                            <button class="btn btn-room btn4 buy_now">PLACE ORDER</button>
                        </div>
                    </div>
                </div>
                <!-- END / CONTENT -->
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>js/jquery-1.12.4.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
     $('body').on('click', '.buy_now', function(e) {
         var totalAmount = <?php echo $data['all_total']; ?>*100;
         var product_id = 1;
         var options = {
             "key": "rzp_test_xP6YCCQfuK1dOY",
             "amount": (1 * totalAmount), 
             "name": "HotelPms",
             "description": "Payment",
             "image": "<?php echo base_url('assets/Product/icon.png');?>",
             "handler": function(response) {
                 console.log(response);
                 $.ajax({
                     url: '<?php echo base_url('hotel-booking-payment');?>',
                     type: 'post',
                     dataType: 'json',
                     data: {
                         razorpay_payment_id: response.razorpay_payment_id,
                         totalAmount: totalAmount,
                         product_id: product_id,
                     },
                     success: function(msg) {
                         window.location.href = '<?php echo base_url('thank-you')?>';
                     }
                 });
             },
             "theme": {
                 "color": "#528FF0"
             }
         };
         var rzp1 = new Razorpay(options);
         rzp1.open();
         e.preventDefault();
     });
 </script>

<!-- END / RESERVATION -->