

<style>
    .thank_you_wrap{
        width: 100%;
        height: 100%;
        margin: 0px auto;
        text-align: center;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        padding: 0px 30px;
    }
    .thank_you_wrap img{
        width: 25%;
        height: auto;
        margin: 0px auto;
    }
    .thank_you_wrap p{
        font-size: 20px;
        color: #000;
        margin-top: 30px;
    }
    .thank_you_wrap a{
        padding: 10px 0px;
        width: 250px;
        background-color: #03164a;
        color: #fff;
        border: none;
        border-radius: 5px;
    }
    header{
        display: none;
    }
    footer{
        display: none;
    }
    @media only screen and (max-width: 768px){
        .thank_you_wrap img{
            width: 40%;
        }
    }
    @media only screen and (max-width: 640px){
        .thank_you_wrap img{
            width: 50%;
        }
    }
    @media only screen and (max-width: 576px){
        .thank_you_wrap img{
            width:70%;
        }
    }
</style>
<?php
  // print_r($data_success);
?>
<div class="thank_you_wrap">
    <img src="<?php echo base_url('assets/images/Reservation/'); ?>header.jpg" alt="">
    <p>Your Room Booking Successfully. Contact to your hotel Support number.</p>
    <a href="<?php echo base_url();?>">Back To Home</a>
</div>