 <!-- BANNER -->
 <section class="banner-tems text-center">
     <div class="container">
         <div class="banner-content">
             <h2>ROOMS & RATES</h2>
         </div>
     </div>
 </section>
 <!-- END-BANNER -->
 <!-- BODY-ROOM-1 -->
 <section class="body-room-1 ">
     <div class="container">
         <div class="room-wrap-1">
             <div class="row">
                 <!-- ITEM -->
                 <?php
                    if ($data) {
                        foreach ($data['data'] as $value) {

                    ?>
                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                             <div class="room-item-1">
                                 <h2><a href="<?php echo base_url('room-details/') . $value['room_id']; ?>">
                                         <?php
                                            if (isset($value['room_name']) && ($value['room_name'] != "N/A") && ($value['room_name'] != NULL)) {
                                                echo '<strong>' . $value['room_no'] . "</strong><br/>" . '<span style="color:#000077;">' . $value['room_name'] . '</span>';
                                            } else {
                                                echo "Room No. " . $value['room_no'];
                                            }     ?>
                                     </a>
                                 </h2>
                                 <div class="img">
                                     <a href="<?php echo base_url('room-details/') . $value['room_id']; ?>"><img src="<?php echo base_url('assets/images/Product/Another-4.jpg'); ?>" alt="#"></a>
                                 </div>
                                 <div class="content">
                                     <?php
                                        if ($value['room_features']) {
                                            $room_feature_array = explode(",", $value['room_features']);
                                            $room_features_string = "";
                                            for ($i = 0; $i < count($room_feature_array); $i++) {
                                                for ($j = 0; $j < count($data['room_fetature_data']); $j++) {
                                                    if ($room_feature_array[$i] == $data['room_fetature_data'][$j]['room_feature_id']) {
                                                        $room_features_string .= $data['room_fetature_data'][$j]['room_feature_name'] . ",";
                                                    }
                                                }
                                            }
                                        } else {
                                            $room_features_string = "";
                                        }
                                        ?>
                                     <p><?php echo $room_features_string; ?></p>
                                     <ul>
                                         <li>Max: <?php echo $value['max_occupancy']; ?> Person(s)</li>
                                         <li>Floor: <?php if ($value['floor_no'] == 0)
                                                        echo 'Ground Floor';
                                                    else if ($value['floor_no'] % 10 == 1)
                                                        echo $value['floor_no'] . 'st Floor';
                                                    else if ($value['floor_no'] % 10 == 2)
                                                        echo $value['floor_no'] . 'nd Floor';
                                                    else if ($value['floor_no'] % 10 == 3)
                                                        echo $value['floor_no'] . 'rd Floor';
                                                    else
                                                        echo $value['floor_no'] . 'th Floor'; ?>
                                         </li>
                                         <li>Rate Type: <?php $rate_type = ($value['floor_no'] == 0) ? "Fixed" : "Depends";
                                                        echo $rate_type;  ?></li>
                                         <li>Bed:
                                             <?php
                                                if ($value['room_bed'] == 1) {
                                                    echo "Single Bed ";
                                                } else if ($value['room_bed'] == 2) {
                                                    echo "Double Bed ";
                                                } else if ($value['room_bed'] == 3) {
                                                    echo "Tripple Bed ";
                                                } else
                                                    echo $value['room_bed'] . " Bed ";

                                                ?>
                                         </li>
                                     </ul>
                                 </div>
                                 <div class="bottom">
                                     <span class="price">Plan <span class="amout"> <?php echo '₹' . $value['room_rent'] . '/day';?></span></span>
                                     <a href="<?php echo base_url('room-details/') . $value['room_id']; ?>" class="btn">VIEW DETAILS</a>
                                 </div>
                             </div>
                         </div>
                 <?php }
                    } else {
                        echo "No room found.";
                    } ?>
                 <!-- END / ITEM -->
             </div>
         </div>
     </div>
 </section>
 <!-- END/BODY-ROOM-1-->