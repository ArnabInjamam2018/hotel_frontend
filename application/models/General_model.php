<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model {

	public function general_function($objArray,$api_url)
    {
//        echo json_encode($headerArray);die;
        $objArray['hotel_id']=$this->config->item('hotel_id');
        $objArray['cid']=$this->config->item('cid');
        // echo json_encode($objArray);die;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST,TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objArray));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        echo json_encode($objArray);die;
//        echo json_encode($headerArray);die;
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    
	public function get_function($api_url)
    {        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$api_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //  curl_setopt($ch,CURLOPT_HEADER, false); 

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }
}
?>