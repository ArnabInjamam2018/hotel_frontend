<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Room extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$api_url = $this->config->item('api_url');
		$get_api_url = $api_url."Api/hotelList";
		
		$data['data'] = json_decode($this->General_model->general_function($_POST,$get_api_url),true);
		//echo "<pre>";print_r($data);die;

		$this->load->view('header');
		$this->load->view('room',$data);
		$this->load->view('footer');
	}

	public function roomDetails($room_id="")
	{
		$api_url = $this->config->item('api_url');
		$get_api_url = $api_url."Api/hotelList";

		$post=array("room_id"=>$room_id);
		$data['data'] = json_decode($this->General_model->general_function($post,$get_api_url),true);
		// echo "<pre>";print_r($data);die;

		$this->load->view('header');
		$this->load->view('room_details',$data);
		$this->load->view('footer');
	}

	public function checkout(){
		$api_url = $this->config->item('api_url');
		$post_api_url = $api_url."Api/frontendhotelbooking";
	

		$_POST['state_id']=29;
		$data['data'] = json_decode($this->General_model->general_function($_POST,$post_api_url),true);
		//echo "<pre>";print_r($data);die;

		if($data['data']['status']==400){
			$this->session->set_flashdata('message', $data['data']['msg']);
			header('location:'.base_url('room-details/'.$_POST['rmid']).'');
			die;
		}

		$this->load->view('header');
		$this->load->view('checkout',$data);
		$this->load->view('footer');
	}

	public function hotelBookingPayment(){

		echo 1;die;

	}
	public function thankyou(){
		$data=[];
		$this->load->view('header');
		$this->load->view('thankyou',$data);
		$this->load->view('footer');
	}


}
